/*
 * Copyright (C) 2019 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.application_name;

import java.net.URL;
import javafx.stage.Stage;
import xal.extension.fxapplication.XalFxDocument;

/**
 *
 * @author Author Name <author.name@email.com>
 */
public class AppDocument extends XalFxDocument {

    AppDocument(Stage stage) {
        super(stage);
        
        // Initialise variables FILETYPE_DESCRIPTION, WILDCARD_FILE_EXTENSION,
        // DEFAULT_FILENAME, and HELP_PAGEID (see xal.extension.fxapplication.XalFxDocument)
    }

    /**
     * Save the ScannerDocument document to the specified URL.
     *
     * @param url The file URL where the data should be saved
     */
    @Override
    public void saveDocumentAs(URL url) {
        // TODO
    }

    /**
     * Reads the content of the document from the specified URL, and loads the
     * information into the application.
     *
     * @param url The path to the XML file
     */
    @Override
    public void loadDocument(URL url) {
        // TODO
    }

}
