# Open XAL Java FX application template
This is a template project for Open XAL applications using Java FX.

It provides the skeleton for the maven module and Gitlab-CI pipeline to build and publish the artifacts to Artifactory.

In order to use it, follow the instruction on https://confluence.esss.lu.se/display/SW/Creating+a+new+Open+XAL+application+using+JavaFX .